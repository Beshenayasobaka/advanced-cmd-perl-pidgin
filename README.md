Advanced CMD Perl Pidgin
========================

Perl plugin for [Pidgin](http://pidgin.im/) that provides additional IM commands. [Original project](https://code.google.com/p/advanced-cmd-perl-pidgin/)

Removed the dependency on Win32 module to make it work across platforms.

##Installation

###Linux
You will need to have `perl` and `perl-modules` installed. Use your package manager to install it or [get it](http://www.perl.org/get.html) from the Perl website

 - Install `Convert::Morse` and `Sys::Load` modules:
 	- ```$ cpanp i Convert::Morse Sys::Load```
 - Place `advanced-cmd.pl` file under your `~/.purple/plugins/` directory
 - (re)start Pidgin 
 - activate `Advanced CMD Plugin in Perl` inside Plugins window (`Tools -> Plugins`)