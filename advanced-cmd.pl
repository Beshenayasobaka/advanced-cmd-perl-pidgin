
use Purple;
use URI::Escape;
use Sys::Load;
use LWP::Simple qw($ua get);
use Convert::Morse qw(as_ascii as_morse is_morsable);

# Changelog
#
# v0.1 - 08.02.2011 (DD.MM.YYYY)
# - Release
#
# v0.2 - 09.02.2011 (DD.MM.YYYY)
# - New command /wp, /wpde, /flood and /wolfram
#
# v0.3 - 12.02.2011 (DD.MM.YYYY)
# - New command /flip
#
# v0.4 - 19.02.2011 (DD.MM.YYYY)
# - New command /myip, /shorturl and for all commands /w[command]
#
# v0.5 - 20.02.2011 (DD.MM.YYYY)
# - New command /horoskop (example "/horoskop tag krebs" or "/horoskop day cancer")
#
# v0.6 - 28.02.2011 (DD.MM.YYYY)
# - New command /forall, /send and /timer
#
# v0.7 - 09.03.2011 (DD.MM.YYYY)
# - New command /brb, /morse, /demorse and /pizza
#
#
# The twelve zodiac signs
# example: english - german (date)
#
# wassermann: Aquarius � Wassermann(21. Januar � 19. Februar)
# fische: Pisces � Fische (20. Februar � 20. M�rz)
# widder: Aries � Widder (21. M�rz � 20. April)
# stier: Taurus � Stier(21. April � 20. Mai)
# zwilling: Gemini � Zwillinge (21. Mai � 21. Juni)
# krebs: Cancer � Krebs (22. Juni � 22. Juli)
# loewe: Leo � L�we (23. Juli � 23. August)
# jungfrau: Virgo� Jungfrau (24. August � 23. September)
# waage: Libra � Waage (24. September � 23. Oktober)
# skorpion: Scorpio � Skorpion (24. Oktober � 22. November)
# schuetze: Sagittarius � Sch�tze (23. November � 21. Dezember)
# steinbock: Capricorn � Steinbock (22. Dezember � 20. Januar)
#
# For lmgtfy or stfw use the stfw plugin!
# Inspired by http://revfad.com/flip.html

my $des = "Example /gidf wort
Words: google googlede yahoo yahoode bing bingde leo gcode 
ebay ebayde ebayuk germanbash amazon amazonde wp wiki wpde 
wikide wolfram sf cpan reverse uptime random steam flip myip 
shorturl horoskop forall timer send brb pizza morse demorse
/flood Hello World;1000";

our %PLUGIN_INFO = (
    perl_api_version => 2,
    name => "Advanced Cmd Plugin in Perl",
    version => "0.7",
    summary => "Einige n�tzliche Kommandos.",
    description => $des,
    author => "-",
    url => "http://code.google.com/p/advanced-cmd-perl-pidgin/",
    load => "plugin_load",
    unload => "plugin_unload",
    prefs_info => "prefs_info_cb",
);

my @pizza = (
	'Benutze ein Telefon mit Tonwahl und dr�cke beliebige Zahlen beim Bestellen. Bitte die Person am anderen Ende, damit sofort aufzuh�ren.',
	'Denke Dir einen Namen f�r eine Kreditkarte aus und frage, ob sie solche Karten akzeptieren.',
	'Benutze die im CB-Funk �blichen Abk�rzungen.',
	'Bestelle ein Maxi-BigM�c-Men�.',
	'Beende das Gespr�ch mit "Und denken Sie daran: Dieses Gespr�ch hat nie stattgefunden!".',
	'Erz�hle dem Pizza-Telefonisten, dass Du auf der anderen Leitung einen anderen Bringdienst hast, und Du das g�nstigste Angebot nehmen wirst.',
	'Gib nur Deine Adresse durch, sage dann "Ach, �berraschen Sie mich einfach" und lege auf.',
	'Beantworte alle Fragen mit Gegenfragen.',
	'Sprich die Namen der Bestellung nicht aus, sondern buchstabiere alles.',
	'Stottere bei jedem "p".',
	'Erkundige Dich, Was Dein Gegen�ber tr�gt.',
	'Sage "Hallo!", warte f�nf Sekunden und tu dann so, als ob sie Dich angerufen h�tten.',
	'Rattere Deine Bestellung in einem Zug herunter. Wenn sie nach Getr�nken fragen, werde panisch und orientierungslos.',
	'Sage dem Telefonisten, dass Du depressiv bist und er Dich aufmuntern soll.',
	'Sprich alle paar Sekunden mit einem anderen Akzent.',
	'Bestelle 52 Peperonischeiben, die nach einer fraktalen Formel angeordnet sind, die Du nun durchgeben willst. Frage nach, ob sie Papier ben�tigen.',
	'Tu so, als w�rdest Du den Telefonisten von irgendwoher kennen. Sage "vom Wetterau-Zeltlager, stimmts?".',
	'Beginne mit "Ich m�chte gerne ...". �ndere Deine Meinung sp�ter und sage "Nein, lieber doch nicht.".',
	'Wenn sie die Bestellung zur Kontrolle wiederholen, dann sage "Ok, das macht 7,80 Euro. Bitte fahren Sie mit dem Wagen bis zur Ausgabe.".',
	'Miete eine Pizza.',
	'Gib Deine Bestellung auf, w�hrend Du neben dem H�rer einen elektrischen Rasierapparat laufen l�sst.',
	'Frage nach, ob Du den Pizzakarton behalten darfst. Wenn sie "ja" sagen, dann sto�e einen Seufzer der Erleichterung aus.',
	'Betone die letzte Silbe von "Peperoni". Sprich mit langem "i".',
	'Bestelle Deine Pizza "gesch�ttelt, nicht ger�hrt".',
	'Frage nach "Sind Sie sicher, dass dies (Pizza-Laden) ist?". Wenn sie es best�tigen, antworte mit "Nun, das m�ssen Sie mir irgendwie beweisen!". Wenn sie Dir best�tigen, dass es wirklich (Pizza-Laden) ist, fange an zu weinen und sage "K�nnen Sie sich vorstellen, wie es ist, angelogen zu werden?"".',
	'Frage nach, ob Deine Pizza auch wirklich tot ist.',
	'�ffe die Stimme des Telefonisten nach.',
	'Lasse irgendwelche Verben beim Sprechen weg.',
	'Rufe an, um Dich �ber den Service zu beschweren. Rufe etwas sp�ter noch mal an und sage, dass Du betrunken warst und es nicht so gemeint h�ttest.',
	'Spiele im Hintergrund auf einer Gitarre.',
	'�berrasche den Telefonisten mit wenig bekannten Fakten �ber Volksmusik.',
	'Frage nach einem kompletten Men�.',
	'Zitiere Shakespeare.',
	'Frage nach, welche Pizzasorte am besten zu einem guten Chardonnay passt.',
	'Belle direkt in den H�rer, dann schimpfe mit Deinem imagin�ren Hund, dass er das gef�lligst lassen soll.',
	'Bestelle keine ganze Pizza, sondern nur ein Viertel.',
	'Erstelle eine Psychoanalyse des Telefonisten.',
	'Frage nach der Telefonnummer des Pizzaservice. Lege den H�rer auf, rufe erneut an und frage noch einmal.',
	'Bestelle zwei verschieden Pizzabel�ge, dann sage "Ach nein, sie werden anfangen gegeneinander zu k�mpfen".',
	'Frage nach dem Telefonisten, der Deine Bestellung beim letzten Mal aufgenommen hat.',
	'Frage dich selbst laut, ob Du Deine Nasenhaare schneiden solltest.',
	'Versuche zu bestellen, w�hrend Du etwas trinkst.',
	'Beginne das Gespr�ch mit "Mein Anruf bei (Pizzaladen), Einstellung 1, Klappe und .. Action!".',
	'Frage nach, ob die Pizza �kologisch angebaut wurde.',
	'Frage nach Pizza-Wartung und -Reparatur.',
	'Benutze Tonwahl und dr�cke w�hrend der Bestellung alle f�nf Sekunden die Tastenfolge 1-1-0.',
	'Sage w�hrend der Bestellung "Ich frag\' mich, was das hier f�r eine Taste ist" und tu so, als ob die Leitung getrennt wird.',
	'Beginne das Gespr�ch damit, das aktuelle Datum vorzulesen und zu sagen "Dies k�nnte der letzte Eintrag sein".',
	'Zische "kschhhhhhhhhhhh" ins Telefon und frage, ob er das gesp�rt hat.',
	'Ergr�nde die Psyche des Telefonisten und nutze den Befund zu Deinem Vorteil.',
	'Nenne als Belag f�r Deine Pizza u. a. den Namen einer anderen Pizza.',
	'Lerne das Mundharmonikaspielen. Unterbrich Deine Bestellung ab und zu, um auf ihr zu spielen. Lass Dich daf�r feiern und loben.',
	'Stelle das Gespr�ch in eine Warteschleife mit Musik.',
	'Bringe dem Telefonisten einen Geheimcode bei. Benutze diesen Code bei allen weiteren Bestellungen.',
	'Bestelle als ersten Belag Champignons. Zum Schluss sage noch "Aber bitte ohne Pilze" und lege auf, bevor sie etwas sagen k�nnen.',
	'Wenn die Bestellung wiederholt wird, �ndere einige Punkte ab. Beim dritten Versuch sagst Du "Sie kriegen es nicht auf die Reihe, stimmts?".',
	'Wenn Du den Preis genannt bekommst, sagst Du "Oh, das klingt kompliziert. Ich hasse Mathematik!".',
	'Bestelle eine 1-Zoll-Pizza.',
	'Frage, wie viele Delphine f�r diese Pizza ihr Leben gelassen haben.',
	'Vermeide das Wort "Pizza" um jeden Preis. Wenn der Telefonist das Wort sagt, sage "Bitte sprechen Sie dieses Wort nicht aus!".',
	'Lasse im Hintergrund einen Krimi mit einer Auto-Verfolgungsjagd laufen. Schreie "Auuu!" wenn geschossen wird.',
	'Notiere den Namen des Telefonisten. Rufe sp�ter genau zur vollen Stunde an, sage "Dies ist ihr XX-Uhr-Weckruf. Wir w�nschen Ihnen einen angenehmen Tag!" und lege auf.',
	'Fange an zu feilschen.',
	'Knacke mit Deinen Fingern direkt in den H�rer.',
	'Auf die Frage "Was m�chten Sie bestellen?" sagst Du "H�h? Sie meinen jetzt?".',
	'Nimm den H�rer immer weiter weg vom Mund. Am Ende des Gespr�ch br�llst Du aus voller Kraft "Tsch�����ss!".',
	'Sage, dass Du erst dann bezahlen kannst, wenn die Leute vom Film zur�ckgerufen haben.',
	'Schlafe mitten im Gespr�ch ein, wache wieder auf und sage "Huch... Wo bin ich? Wer sind Sie??".',
	'Sage mit Deiner rauchigsten Stimme "H�r auf, mir Mist �ber Ern�hrung zu erz�hlen, sondern sag mir lieber ob ihr etwas exotisch-s�ndiges habt...".',
	'Verlange, dass diesmal der Teig oben sein soll.',
	'Kreische mit �berschlagender Stimme "Verr�cktes Backofenzeug".',
	'Frage nach einem Angebot, das es nur bei einem anderen Bringservice gibt (z. B. Pizza-Hut).',
	'Mache eine Liste von �u�erst exotischen Speisen und bestelle sie als Belag (z. B. Sushi).',
	'Erz�hle, dass es Dein Hochzeitstag sei und dass Du es sch�tzen w�rdest, wenn der Pizzabote sich hinter dem Sofa versteckt und dort als �berraschung hervorspringt, wenn Dein/e Gatte/in heimkommt.',
	'Fordere Deinen Gegen�ber auf, seinem Vorgesetzten mitzuteilen, dass dessen Chef gefeuert ist.',
	'Gib der Person am anderen Ende ein Bagatelldelikt zu Protokoll.',
	'Wenn er etwas vorschl�gt, verk�nde unnachgiebig "Ich soll nicht durch den s��en Klang Deiner Worte in Versuchung gef�hrt werden!".',
	'Sei ungenau bei Deiner Bestellung.',
	'Wenn er die Bestellung wiederholt, sage "Noch mal bitte, aber diesmal mit etwas mehr "Oooooohhhhhh!".',
	'Verk�nde Deine Bestellung und sage "Weiter werde ich in der Beziehung mit Ihnen nicht gehen!".',
	'Frage, ob ihm der Ausdruck "die Pizza versohlen" gel�ufig ist. Falls nicht, erkl�re genau, wie man es macht und verlange, dass dies auch mit Deiner Pizza geschieht.',
	'Frage, ob sie Deine Bestellung mit auf die Karte �bernehmen wollen. Schlage einen fairen Deal vor.',
	'Ahme die Stimme eines Stars nach und betone dann bei Deiner Bestellung, dass Du einen Dreck von einem grobschl�chtigen, unf�higen, pickelgesichtigen Anf�ngerdeppen entgegennehmen wirst.',
	'Bestelle bei ihm eine Lebensversicherung f�r die Dauer des Pizzagenusses.',
	'Wenn er fragt "War das alles?", kichere und sage "Das werden wir schon noch rausfinden, oder etwa nicht?".',
	'W�hrend Du telefonierst, solltest Du das Eintreten in die Pubert�t mit einem Kieksen wie im Stimmbruch nachahmen. Verhalte dich sehr aufgeregt.',
	'Engagiere Dich in einem tiefsinnigen Gedankenaustausch.',
	'Wenn Dein Gegen�ber eine zus�tzliche Bestellung vorschl�gt, frage, warum er Dich derma�en straft.',
	'Frage, ob die Pizza bereits ihre Impfungen erhalten hat.',
	'Bestelle eine ged�nstete oder gekochte Pizza.',
	'Verlange ausdr�cklich den Auslieferer vom letzten mal, begr�nde dies mit "der kann am besten steppen (strippen, tanzen, jodeln, Staubsaugen o. �.)".',
	'Behaupte, Du seiest allergisch gegen K�se und Tomaten, welche Pizza man Dir da empfehlen k�nne.',
	'Wiederhole die ganze Zeit �ber alles, was Dein Gegen�ber sagt. Bleib hart, halte durch, bis er entnervt auflegt. Rufe noch mal an und mit einem "Oh, wir sind wohl getrennt worden" kann der Spa� erneut beginnen.',
	'Frage nach, wie viele Kalorien die Pizza hat, heule dann verzweifelt auf und fl�stere "Ich bin zu dick, ich bin zu dick", verlange nach einer Pizza mit unter 200 Kalorien.',
	'Schimpfe, er solle mit dieser perversen Sauerei aufh�ren, w�hrend er mit Dir telefoniert.',
	'Sage sage einfach einfach jedes jedes Wort Wort doppelt doppelt.',
	'Fange mitten im Gespr�ch an zu Kichern und frage den Telefonisten, ob er auch diese Stimmen h�rt.',
	'Bet�tige w�hrend des Telefonates mehrmals die Klosp�lung.',
	'Sage einfach gar nichts, sondern warte, bis jemand den H�rer abnimmt und knistere dann mit einer Plastikt�te. Dazu kannst Du leise pfeifen.',
	'Erschrecke, wenn du die Stimme deines Gegen�bers h�rst und behaupte, dass du einen Schatten in seiner Zukunft gesehen hast.'
);

my @brb = (
	"I am being viciously attacked by a maths book, please excuse me while I go and extract myself.",
	"My fridge is rampaging in next doors garden, I need to go cheer it on.",
	"My finger is stuck in the plug hole.",
	"My chiwawa has broken free and is eating kitchen knives.",
	"God has summoned me to the toilet.",
	"I have had an idea and need to feed it.",
	"The house is on fire.",
	"The neighbours have broken out of the padded room.",
	"The window's melting.",
	"My hair has internally combusted.",
	"My brain has been sighted running across the road.",
	"The hedgehog is skating in the sink again.",
	"Time to heat my watch.",
	"Must start the cat spin cycle.",
	"Seems my walls need ironing..",
	"Going to Reno.",
	"Lounging in bliss.",
	"There seems to be a problem with my toenails.",
	'*insert lame excuse here*',
	"Smoking pottery.",
	"Driving screws. Really fast!",
	"Connecting my neural networking hair extensions.",
	"My neighbour is complaining about cement in his mail box.",
	"Making modem noises for fun.",
	"Inventing spontaneous combustion.",
	"Hacking mainframes and shit.",
	"Gonna get some new shoes for my donkey.",
	"Trying to figure out the official AIM client... Don't wait up.",
	"Busy trying to cure my goldfish from its fear of water.",
	"The FBI just called, I'm wanted for a special mission.",
	"Doing brain surgery on myself... again.",
	"Memorising Chinese characters by heart... all 50.000 of them.",
	"Funny you should write me just now, I just /happen/ to be on my way out.",
	"Got a cake in the oven I need to check up on.",
	"Trying to convince Bill Gates to give up the whole Windows-concept, since it's not going anywhere.",
	"Trying to figure out how to give Paris Hilton the image of being an intelligent blond.",
	"Cracking codes for the CIA...",
	"Busy planning how to take over the world.",
	"Practising for the world pickled cow-tongue eating championship.",
	"The scabies are breeding again, gotta find some tweezers.",
	"I can see my neighbour undressing and oiling himself up, gotta stop him from wrestling my dog again.",
	"Both hands are busy... don't ask.",
	"The slaves are revolting again",
	"My sister is screaming at the imaginary gnomes again.",
	"I think I may have a third nipple.",
	"Going to talk to the Jehovah's Witnesses at my door naked.",
	"Planting apricot trees in my orchard.",
	"Getting beamed up by Scotty",
	"Can't sit down. Anyone have any toilet paper?",
	"Pea up the nostril again.",
	"Putting pinholes in my mate's condoms for an April Fool.",
	"Putting on my wetsuit to surf the web",
	"Brushing teeth. Chick was a dude.",
	"Recycling corn in bathroom.",
	"Throwing the ball around with my quadriplegic son.",
	"Need more peanuts for my pet elephant.",
	"Seriously, I'm almost out of minutes.",
	"Boosting my reproductive capacity via beer consumption.",
	"Corresponding with the dead.",
	"Putting on beer goggles.",
	"Fabricating a rubber donkey.",
	"Trying to levitate.",
	"Where's my lighter? I can feel a huge fart coming.",
	"Sharpening my blugeon.",
	"Combing my back-hair.",
	"Time for a full body shave.",
	"Mud wrestling a peacock.",
	"My penguin is giving birth.",
	"Just fallen through a crack in the space/time continuum. Leave me a message and I'll get back to you last week",
	"You don't want to know.",
	"I'm dancing with myself - oh oh oh oh"
);

my %hashreverse = (
	a => "\x{0250}",
	b => 'q',
	c => "\x{0254}", 
	d => 'p',
	e => "\x{01DD}",
	f => "\x{025F}", 
	g => "\x{0183}",
	h => "\x{0265}",
	i => "\x{0131}", 
	j => "\x{027E}",
	k => "\x{029E}",
	m => "\x{026F}",
	n => 'u',
	r => "\x{0279}",
	t => "\x{0287}",
	v => "\x{028C}",
	w => "\x{028D}",
	y => "\x{028E}",
	'.' => "\x{02D9}",
	'[' => ']',
	'(' => ')',
	'{' => '}',
	'?' => "\x{00BF}", 
	'!' => "\x{00A1}",
	"\'" => ',',
	'<' => '>',
	'_' => "\x{203E}",
	';' => "\x{061B}",
	"\x{203F}" => "\x{2040}",
	"\x{2045}" => "\x{2046}",
	"\x{2234}" => "\x{2235}",
	'\r' => '\n' 
);

sub replacement {
	if (exists($hashreverse{$_})) {
		$hashreverse{$_};
	} else {
		$_;
	}
}

sub flip_string {
	my @chars = split(//, reverse lc shift);
	@chars = map(replacement, @chars);
	join('', @chars);
}


sub plugin_init {
	return %PLUGIN_INFO;
}

sub reverse_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = reverse($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wreverse_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = reverse($args[0]);
	$conv->write("", umlaut2txt($msg), Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub pizza_message_cb {
	my($conv, $cmd, $plugin, @args) = @_;

	my $msg;
	for(1..1000){
		srand();
		$msg = $pizza[int(rand(scalar(@pizza)))];
		if($msg =~ /\w+/){
			$msg = 'Bei der Pizzabestellung: '.$msg ;
			last;
		}
	}
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send(umlaut2txt($msg));
	return Purple::Cmd::Ret::OK;
}

sub wpizza_message_cb {
	my($conv, $cmd, $plugin, @args) = @_;

	my $msg;
	for(1..1000){
		srand();
		$msg = $pizza[int(rand(scalar(@pizza)))];
		if($msg =~ /\w+/){
			$msg = 'Bei der Pizzabestellung: '.$msg ;
			last;
		}
	}

	$conv->write("", umlaut2txt($msg), Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub brb_message_cb {
	my($conv, $cmd, $plugin, @args) = @_;

	my $msg;
	for(1..1000){
		srand();
		$msg = $brb[int(rand(scalar(@brb)))];
		if($msg =~ /\w+/){
			last;
		}
	}
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send(umlaut2txt($msg));
	return Purple::Cmd::Ret::OK;
}

sub wbrb_message_cb {
	my($conv, $cmd, $plugin, @args) = @_;

	my $msg;
	for(1..1000){
		srand();
		$msg = $brb[int(rand(scalar(@brb)))];
		if($msg =~ /\w+/){
			last;
		}
	}

	$conv->write("", umlaut2txt($msg), Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub flood_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my($message,$number) = split(/;/,join("",@args));
	if($message && $number =~ /^\d+$/ && $number > 0){
		my $msg;
		foreach (1..$number){
			$msg .= $message . "\n";
		}
		my $sendmsg = $conv->get_im_data();
		$sendmsg->send($msg);
		return Purple::Cmd::Ret::OK;
	}else{
		return Purple::Cmd::Return::FAILED;
	}
}

sub wflood_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my($message,$number) = split(/;/,join("",@args));
	if($message && $number =~ /^\d+$/ && $number > 0){
		my $msg;
		foreach (1..$number){
			$msg .= $message . "\n";
		}
		$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
		return Purple::Cmd::Ret::OK;
	}else{
		return Purple::Cmd::Return::FAILED;
	}
}


sub random_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	$args[0] =~ s/[^\d]//g;

	if($args[0] !~ /^\d+$/){
		$args[0] = Purple::Prefs::get_int('/plugins/core/autocmdperl/random');
	}
	srand();
	my $msg = int(rand($args[0]));
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send("Zufallszahl " . $msg);
	return Purple::Cmd::Ret::OK;
}

sub wrandom_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	$args[0] =~ s/[^\d]//g;

	if($args[0] !~ /^\d+$/){
		$args[0] = Purple::Prefs::get_int('/plugins/core/autocmdperl/random');
	}
	srand();
	my $msg = int(rand($args[0]));
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub morse_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = as_morse($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wmorse_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = as_morse($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub demorse_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = as_ascii($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send(lc($msg));
	return Purple::Cmd::Ret::OK;
}

sub wdemorse_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = as_ascii($args[0]);
	$conv->write("", lc($msg), Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub gidf_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.gidf.de/'.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wgidf_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.gidf.de/'.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub google_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.google.com/search?q='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wgoogle_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.google.com/search?q='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub googlede_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.google.de/search?q='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wgooglede_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.google.de/search?q='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub bing_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.bing.com/search?q='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wbing_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.bing.com/search?q='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub bingde_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.bing.de/search?q='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub bingde_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.bing.de/search?q='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub cpan_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://search.cpan.org/search?query='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wcpan_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://search.cpan.org/search?query='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub yahoo_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://search.yahoo.com/?p='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wyahoo_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://search.yahoo.com/?p='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub yahoode_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://de.search.yahoo.com/?p='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wyahoode_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://de.search.yahoo.com/?p='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub wiki_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://en.wikipedia.org/wiki/'.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wwiki_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://en.wikipedia.org/wiki/'.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub wikide_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://de.wikipedia.org/wiki/'.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wwikide_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://de.wikipedia.org/wiki/'.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub amazon_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.amazon.com/s?field-keywords='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wamazon_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.amazon.com/s?field-keywords='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub amazonde_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.amazon.de/s?field-keywords='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wamazonde_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.amazon.de/s?field-keywords='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub sf_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://sourceforge.net/search/?q='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wsf_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://sourceforge.net/search/?q='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub gcode_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://code.google.com/intl/de-DE/query/#q='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wgcode_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://code.google.com/intl/de-DE/query/#q='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub leo_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://dict.leo.org/?search='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wleo_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://dict.leo.org/?search='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub ebay_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://shop.ebay.com/i.html?_nkw='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub webay_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://shop.ebay.com/i.html?_nkw='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub ebayde_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://shop.ebay.de/i.html?_nkw='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub webayde_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://shop.ebay.de/i.html?_nkw='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub ebayuk_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://shop.ebay.co.uk/i.html?_nkw='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub webayuk_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://shop.ebay.co.uk/i.html?_nkw='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub germanbash_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://german-bash.org/?searchtext='.uri_escape($args[0]) . '&search_in=both&action=search_';
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wgermanbash_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://german-bash.org/?searchtext='.uri_escape($args[0]) . '&search_in=both&action=search_';
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub steam_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://store.steampowered.com/search/?term='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wsteam_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://store.steampowered.com/search/?term='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub wolfram_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.wolframalpha.com/input/?i='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wwolfram_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.wolframalpha.com/input/?i='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}


sub phpclasses_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.phpclasses.org/search.html?words='.uri_escape($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wphpclasses_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $msg = 'http://www.phpclasses.org/search.html?words='.uri_escape($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub flip_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = flip_string($args[0]);
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wflip_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = flip_string($args[0]);
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub myip_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $tempsite = get("http://www.cmyip.com/");
	my($temptitle) = ($tempsite =~ /<title>([^<]*)<\/title>/);
	my $msg = (split(/\s+/,$temptitle))[3];
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wmyip_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $tempsite = get("http://www.cmyip.com/");
	my($temptitle) = ($tempsite =~ /<title>([^<]*)<\/title>/);
	my $msg = (split(/\s+/,$temptitle))[3];
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub shorturl_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = get('http://is.gd/api.php?longurl='.uri_escape(join("",@args)));
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wshorturl_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = get('http://is.gd/api.php?longurl='.uri_escape(join("",@args)));
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub uptime_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $ticks = Sys::Load::uptime();# Only Win32
	my $updays = int($ticks/86400);
	my $uphrs  = int(($ticks = ($ticks - $updays*86400)) /3600);
	my $upmin  = int(($ticks = ($ticks - $uphrs*3600)) /60);
	$updays = 0 unless($updays);
	$upmin = 0 unless($upmin);
	$uphrs = 0 unless($uphrs);

	my $msg = "Uptime: $updays days, $uphrs hours, $upmin minutes";
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub wuptime_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;

	my $ticks = Sys::Load::uptime();# Only Win32
	my $updays = int($ticks/86400);
	my $uphrs  = int(($ticks = ($ticks - $updays*86400)) /3600);
	my $upmin  = int(($ticks = ($ticks - $uphrs*3600)) /60);
	$updays = 0 unless($updays);
	$upmin = 0 unless($upmin);
	$uphrs = 0 unless($uphrs);

	my $msg = "Uptime: $updays days, $uphrs hours, $upmin minutes";
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub plugin_load {
	my $plugin = shift;
	my $conv = Purple::Conversations::get_handle();

	Purple::Cmd::register($plugin, "reverse", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&reverse_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wreverse", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wreverse_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "random", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&random_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wrandom", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wrandom_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "gidf", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&gidf_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wgidf", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wgidf_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "google", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&google_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wgoogle", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wgoogle_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "googlede", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&googlede_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wgooglede", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wgooglede_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "gde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&googlede_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wgde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wgooglede_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "bing", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&bing_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wbing", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wbing_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "bingde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&bingde_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wbingde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wbingde_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "cpan", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&cpan_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wcpan", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wcpan_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "yahoo", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&yahoo_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wyahoo", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wyahoo_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "yahoode", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&yahoode_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wyahoode", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wyahoode_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wiki", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wiki_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wwiki", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wwiki_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wp", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wiki_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wwp", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wwiki_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wpde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wikide_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wwpde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wwikide_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wikide", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wikide_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wwikide", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wwikide_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "amazon", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&amazon_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wamazon", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wamazon_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "amazonde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&amazonde_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wamazonde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wamazonde_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "sf", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&sf_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wsf", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wsf_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "gcode", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&gcode_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wgcode", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wgcode_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "leo", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&leo_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wleo", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wleo_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "ebay", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&ebay_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "webay", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&webay_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "ebayde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&ebayde_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "webayde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&webayde_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "ebayuk", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&ebayuk_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "webayuk", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&webayuk_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "germanbash", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&germanbash_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wgermanbash", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wgermanbash_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "gbash", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&germanbash_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wgbash", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wgermanbash_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "bashde", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&germanbash_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "uptime", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&uptime_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wuptime", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wuptime_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "steam", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&steam_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wsteam", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wsteam_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "phpclasses", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&phpclasses_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wphpclasses", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wphpclasses_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wolfram", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wolfram_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wwolfram", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wwolfram_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "flood", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&flood_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "flip", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&flip_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wflip", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wflip_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "myip", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&myip_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wmyip", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wmyip_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "shorturl", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&shorturl_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wshorturl", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wshorturl_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "horoskop", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&horoskop_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "whoroskop", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&whoroskop_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "pizza", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&pizza_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wpizza", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wpizza_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "morse", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&morse_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wmorse", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wmorse_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "demorse", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&demorse_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wdemorse", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wdemorse_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "brb", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&brb_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wbrb", "", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wbrb_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "send", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&send_message_cb,"",$plugin);
	Purple::Cmd::register($plugin, "wsend", "s", Purple::Cmd::Priority::DEFAULT,
			Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT,
			0, \&wsend_message_cb,"",$plugin);

	my $helpforall = "Execute some command for all open conversations.\n
EXAMPLE:
'/forall -c topic' -- Executes the command 'topic' in all the open chats, thus showing the topic of each channel (in the channel's tab).
'/forall -s gotta go!' -- Send 'gotta go!' in all the open IMs.
'/forall -i say gotta go!' -- Executes the command 'say gotta go!' in all the open IMs, which sends 'gotta go!' to the open IMs.
'/forall clear' -- Executes the command 'clear' in all the open conversations, thus clearing the backlog in each of them.
";
	Purple::Cmd::register($plugin, "forall", "ws", Purple::Cmd::Priority::DEFAULT, Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT | Purple::Cmd::Flag::ALLOW_WRONG_ARGS, 0, \&forall_cmd_cb, "$helpforall", $plugin);

	my $timerhelp = "TIMER <seconds> <command>
Executes <command> after <seconds> seconds.\n
EXAMPLE:\n
/timer 5 say Hi
	- Executes '/say Hi' in the current conversation after 5 seconds\n
/timer -c 5 say Hi
	- Executes '/say Hi' in the current conversation after every 5 seconds\n
/timer
	- Lists the commands set to execute. The commands are listed in '<id> : <command>' format.\n
/timer -d <id> [<id> <id> ...]
	- Delete the listed commands";
	$help =~ s/</&lt;/g;
	$help =~ s/>/&gt;/g;
	my $id = Purple::Cmd::register($plugin, "timer", "wws", Purple::Cmd::Priority::DEFAULT, Purple::Cmd::Flag::IM | Purple::Cmd::Flag::CHAT | Purple::Cmd::Flag::ALLOW_WRONG_ARGS, 0, \&timer_cmd_cb, "$timerhelp", $plugin);

	while (($k, $v) = each %hashreverse) {
		$hashreverse{$v} = $k;
	}

	&dbmsg("plugin_load() - Auto Cmd Plugin in Perl loaded.");

	Purple::Prefs::add_none( "/plugins/core/autocmdperl");
	Purple::Prefs::add_int( "/plugins/core/autocmdperl/random", 100);
}

sub forall_cmd_cb {
	my($conv, $cmd, $data, @args) = @_;
	my $len = scalar(@args);

	my @convs = ();
	if($args[0] eq "-s"){
		shift(@args);
		@convs = Purple::get_ims();
		$cmd = join(" ", @args);

		foreach (@convs){
			$_->get_im_data->send($cmd);
		}
	}elsif($args[0] eq "-c"){
		shift(@args);
		@convs = Purple::get_chats();

		$cmd = join(" ", @args);
		foreach (@convs){
			$_->do_command($cmd, NULL);
		}
	}else{
		if($args[0] eq "-i"){
			shift(@args);
			@convs = Purple::get_ims();

			foreach (@convs){
				$_->do_command($cmd, NULL);
			}
		}else{
			@convs = Purple::get_conversations();

			$cmd = join(" ", @args);
			foreach (@convs){
				$_->do_command($cmd, NULL);
			}
		}
	}
	return Purple::Cmd::Return::OK;
}

sub send_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = $args[0];
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Return::OK;
}

sub wsend_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	my $msg = $args[0];
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Return::OK;
}

sub horoskop_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	@args = split(/\s+/,join(" ",@args));

	$args[0] =~ s/day/tag/ig;
	$args[0] =~ s/wochen/woche/ig;
	$args[0] =~ s/weeks/woche/ig;
	$args[0] =~ s/week/woche/ig;
	$args[1] =~ s/aquarius/wassermann/ig;
	$args[1] =~ s/pisces/fische/ig;
	$args[1] =~ s/aries/widder/ig;
	$args[1] =~ s/taurus/stier/ig;
	$args[1] =~ s/gemini/zwilling/ig;
	$args[1] =~ s/cancer/krebs/ig;
	$args[1] =~ s/leo/loewe/ig;
	$args[1] =~ s/virgo/jungfrau/ig;
	$args[1] =~ s/libra/waage/ig;
	$args[1] =~ s/scorpio/skorpion/ig;
	$args[1] =~ s/sagittarius/schuetze/ig;
	$args[1] =~ s/capricorn/steinbock/ig;
	my($msg) = horoskop(lc($args[1]),lc($args[0]));
	my $sendmsg = $conv->get_im_data();
	$sendmsg->send($msg);
	return Purple::Cmd::Ret::OK;
}

sub whoroskop_message_cb {
	my ($conv, $cmd, $plugin, @args) = @_;
	@args = split(/\s+/,join(" ",@args));

	$args[0] =~ s/day/tag/ig;
	$args[0] =~ s/wochen/woche/ig;
	$args[0] =~ s/weeks/woche/ig;
	$args[0] =~ s/week/woche/ig;
	$args[1] =~ s/aquarius/wassermann/ig;
	$args[1] =~ s/pisces/fische/ig;
	$args[1] =~ s/aries/widder/ig;
	$args[1] =~ s/taurus/stier/ig;
	$args[1] =~ s/gemini/zwilling/ig;
	$args[1] =~ s/cancer/krebs/ig;
	$args[1] =~ s/leo/loewe/ig;
	$args[1] =~ s/virgo/jungfrau/ig;
	$args[1] =~ s/libra/waage/ig;
	$args[1] =~ s/scorpio/skorpion/ig;
	$args[1] =~ s/sagittarius/schuetze/ig;
	$args[1] =~ s/capricorn/steinbock/ig;
	my($msg) = horoskop(lc($args[1]),lc($args[0]));
	$conv->write("", $msg, Purple::Conversation::Flags::NO_LOG, 0);
	return Purple::Cmd::Ret::OK;
}

sub plugin_unload {
	my $plugin = shift;
	&dbmsg("plugin_unload() - Auto Cmd Plugin in Perl unloaded.");
}

sub prefs_info_cb {
	my ($frame, $ppref);
	$frame = Purple::PluginPref::Frame->new();

	$frame->add(Purple::PluginPref->new_with_label("Auto Cmd in Perl"));

	$ppref = Purple::PluginPref->new_with_name_and_label(
		"/plugins/core/autocmdperl/random", "Vorgabe Zufall ");
	$ppref->set_max_length(10);
	$ppref->set_bounds(1, 9999999);
	$frame->add($ppref);

	return $frame;
}

sub dbmsg {
	my $msg = shift;
	Purple::Debug::misc("autocmdperl", $msg."\n");
}

sub horoskop {
	my $zwei = shift || 'widder';#german
	my $erst = shift || 'tag';#german
	my $text;

	if($erst eq "woche"){
		my $url = get("http://de.lifestyle.yahoo.com/sterne/wochenhoroskop-$zwei.html");
		($text) = ($url =~ /(<p><b>Gesundheit\/Fitness<\/b>.+?)"Lust"-Partner:/s);
		$text =~ s/<(.+?)>//g;
		$text =~ s/[\n\r]//g;
		$text =~ s/&nbsp;Beruf\/Geld\s*/\n\nBeruf\/Geld\n/g;
		$text =~ s/Gesundheit\/Fitness\s*/Gesundheit\/Fitness\n/g;
		$text =~ s/&nbsp;Liebe\/Partnerschaft\s*/\n\nLiebe\/Partnerschaft\n/g;
	}else{
		my $url = get("http://de.lifestyle.yahoo.com/sterne/horoskop-$zwei.html");
		($text) = ($url =~ /height=12 width=12 alt=stern>([^><]*)<!-- daily horoscope daily \d+ -->/s);
		$text =~ s/[\n\r]//g;
	}

	return(umlaut2txt($text));#Germantext
}

sub umlaut2txt {
	my($text) = @_;
	$text =~ s/�/ae/g;
	$text =~ s/�/ue/g;
	$text =~ s/�/oe/g;
	$text =~ s/�/Ae/g;
	$text =~ s/�/Ue/g;
	$text =~ s/�/Oe/g;
	$text =~ s/�/ss/g;
	return($text);
}

sub timer_cmd_cb {
	my($conv, $cmd, $plugin, @args) = @_;
	my $len = scalar(@args);

	if($len == 1){
		return Purple::Cmd::Return::FAILED;
	}

	if($len >= 2){
		my %data = ();
		if($args[0] eq '-d'){
			my @jobs = split(/[ ,;]/, $args[1]);
			foreach my $job (@jobs) {
				Purple::timeout_remove($job);
				delete $timers{$job + 0};
			}
			return Purple::Cmd::Return::OK;
		}elsif ($args[0] eq '-c'){
			$data{'continue'} = 1;
			shift @args;
		}else{
			$data{'continue'} = 0;
		}
		$data{'timer'} = shift @args;
		$data{'command'} = join(' ', @args);
		$data{'creation'} = time;
		$data{'conv'} = $conv;
		my $tm = Purple::timeout_add($plugin, $data{'timer'}, \&timer_execute_command, \%data);
		$data{'timeout'} = $tm;
		$timers{$tm} = \%data;
	}else{
		my $output = "List of timeout commands:\n";
		while(my ($tm, $data) = each(%timers)){
			$output .= "\t" . $data->{'timeout'} . ": '" . $data->{'command'} . "' after " . ($data->{'continue'} ? "every " : "") . $data->{'timer'} . " seconds\n";
		}
		$conv->write("", $output, Purple::Conversation::Flags::NO_LOG, 0);
	}
	return Purple::Cmd::Return::OK;
}

sub timer_execute_command {
	my $data = shift;
	my $conv = $data->{'conv'};
	$conv->do_command($data->{'command'}, NULL);
	if ($data->{'continue'}) {
		return 1;
	}else{
		delete $timers{$data->{'timeout'}};
		return 0;
	}
}